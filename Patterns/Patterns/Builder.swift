protocol Builder {
    func setSprite() -> Void
    func setBehavior(behavior: String) -> Void
    func setColor(color: String) -> Void
}

class Koopa {
    var sprite: String = ""
    var behavior: String = ""
    var color: String = ""
}

class Goomba {
    var sprite: String = ""
    var behavior: String = ""
    var color: String = ""
}

class KoopaBuilder: Builder {
    private var koopa = Koopa()

    func setSprite() -> Void {
        koopa.sprite = ".^.~" 
    }
    func setBehavior(behavior: String) -> Void {
        koopa.behavior = behavior
    }
    func setColor(color: String) -> Void{
        koopa.color = color
    }

    func getProduct() -> Koopa{
        return koopa
    }

}

class GoombaBuilder: Builder {
    private var goomba = Goomba()
    func setSprite() -> Void {
        goomba.sprite = "(._.)"
    }
    func setBehavior(behavior: String) -> Void {
        goomba.behavior = behavior
    }
    func setColor(color: String) -> Void {
        goomba.color = "Brown"
    }
    func getProduct() -> Goomba{
        return goomba
    }
}

class Director {
    var builder: Builder?

    init (builder: Builder){
        self.builder = builder
    }

    func createGreenKoopa(){
        builder?.setSprite()
        builder?.setBehavior(behavior: "Move forward, bump objects")
        builder?.setColor(color: "Green")
    }

    func createGoomba(){
        builder?.setSprite()
        builder?.setBehavior(behavior: "Move forward, bump stuff")
        builder?.setColor(color: "Brown")
    }
}

func run() {
    var goombaBuilder = GoombaBuilder()
    var koopaBuilder = KoopaBuilder()
    var director = Director(builder: goombaBuilder)
    director.createGoomba()
    var goomba = goombaBuilder.getProduct()
    print(goomba.sprite, goomba.behavior, goomba.color)
    director = Director(builder: koopaBuilder)
    director.createGreenKoopa()
    var koopa = koopaBuilder.getProduct()
    print(koopa.sprite, koopa.behavior, koopa.color)
}