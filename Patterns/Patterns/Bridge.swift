protocol DataSerializer {
    func serializeInt(key: String, value: Int) -> String
    func serializeString(key: String, value: String) -> String
    func serializeFloat(key: String, value: Float) -> String
}

class DataManager {
    private var dataSerializer: DataSerializer

    init(dataSerializer: DataSerializer) {
        self.dataSerializer = dataSerializer
    }

    func manageInteger(key: String, value: Int) {
        print(dataSerializer.serializeInt(key: key, value: value))
    }

    func manageString(key: String, value: String) {
        print(dataSerializer.serializeString(key: key, value: value))
    }

    func manageFloat(key: String, value: Float) {
        print(dataSerializer.serializeFloat(key: key, value: value))
    }
}

class XMLSerializer: DataSerializer {
    func serializeInt(key: String, value: Int) -> String {
        return "<\(key)><int>\(value)</int></\(key)>"
    }
    func serializeString(key: String, value: String) -> String {
        return "<\(key)><string>\(value)</string></\(key)>"
    }
    func serializeFloat(key: String, value: Float) -> String {
        return "<\(key)><float>\(value)</float></\(key)>"
    }
}

class JSONSerializer: DataSerializer {
    func serializeInt(key: String, value: Int) -> String {
        return "\(key): \(value)"
    }
    func serializeString(key: String, value: String) -> String {
        return "'\(key)': '\(value)'"
    }
    func serializeFloat(key: String, value: Float) -> String {
        return "'\(key)': \(value)"
    }
}

func bridgeExample() {
    var json = JSONSerializer()
    var xml = XMLSerializer()
    var jsonManager = DataManager(dataSerializer: json)
    var xmlManager = DataManager(dataSerializer: xml)
    jsonManager.manageInteger(key: "size", value: 5) 
    xmlManager.manageInteger(key: "size", value: 5)
}