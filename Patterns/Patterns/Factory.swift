//
//  Factory.swift
//  Patterns
//
//  Created by Juan Sulca on 4/22/19.
//  Copyright © 2019 EPN. All rights reserved.
//

protocol GhostFactory {
    
    func create() -> Ghost
    
    func action() -> Void
    
}

extension GhostFactory {
    func action() -> Void {
        let product = create()
        product.chase()
    }
}

protocol Ghost {
    
    func chase() -> Void
    
}

class Blinky: Ghost {
    func chase() {
        print("Chasing as Blinky...👻")
    }
}

class Clyde: Ghost {
    func chase() {
        print("Chasing as Clyde...👻")
    }
}

class BlinkyFactory: GhostFactory {
    func create() -> Ghost {
        return Blinky()
    }
}

class ClydeFactory: GhostFactory {
    func create() -> Ghost {
        return Clyde()
    }
}


func moveSprite(factory: GhostFactory) {
    factory.action()
}
