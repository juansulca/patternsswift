protocol Observer: class {
    func update(subject: WeatherPrediction)
}

class WeatherPrediction {
    
    var state = ["rainy", "sunny", "cloudy"].randomElement()
    
    private lazy var observers = [Observer]()
    
    func attach(_ observer: Observer) {
        observers.append(observer)
    }
    
    func dettach(_ observer: Observer) {
        if let id = observers.firstIndex(where: { $0 === observer}) {
            observers.remove(at: id)
        }
    }
    
    func notifyObservers() {
        observers.forEach{ $0.update(subject: self) }
    }
    
    func businessLogic(){
        print("Executing some business logic") 
        state = ["rainy", "sunny", "cloudy"].randomElement()
        print("Changed state to: \(state ?? "none")")
        notifyObservers()
    }
    
}

class SunnyObserver: Observer {
	func update(subject: WeatherPrediction){
        if(subject.state == "sunny") {
            print("SunnyObserver: What a great day! ☀")
        }
    }    
}

class CloudyObserver: Observer {
	func update(subject: WeatherPrediction){
        if(subject.state == "rainy" || subject.state == "cloudy") {
            print("CloudyObserver: This is so sad! 🌧")
        }
    }    
}


func main() {
    let weatherPrediction = WeatherPrediction()
    let sunny = SunnyObserver()
    let cloudy = CloudyObserver()
    weatherPrediction.attach(sunny)
    weatherPrediction.attach(cloudy)
    
    weatherPrediction.businessLogic()
    weatherPrediction.businessLogic()
    weatherPrediction.dettach(cloudy)
    weatherPrediction.businessLogic()
    weatherPrediction.dettach(sunny)
}

main()