import Foundation

class SessionController {
    private var id: Int

    static var instance: SessionController = {
        let instance = SessionController()
        return instance
    }()

    private init(){
        id = Int.random(in: 0..<64)
    }

    func logIn(id: Int){
        self.id = id 
    }

    func getUserId(){
        print("logged in as user: \(id)")
    }
}


func testSingleton(){
    var s = SessionController.instance
    var s2 = SessionController.instance
    s.logIn(id:64)
    s.getUserId()
    s2.getUserId()
}