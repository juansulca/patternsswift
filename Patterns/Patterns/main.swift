//
//  main.swift
//  Patterns
//
//  Created by Juan Sulca on 4/22/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation

print("Factory Pattern")
moveSprite(factory: BlinkyFactory())
moveSprite(factory: ClydeFactory())
