import Foundation

protocol Field {

    func save() -> String

}

class EmailField: Field {

    func save() -> String {
        return "Saving EmailField..."
    }

}

class FieldDecorator: Field {

    private var field: Field

    init(_ field: Field) {
        self.field = field
    }

    func save() -> String {
        return field.save()
    }
}

class EmailValidatioDecorator: FieldDecorator {
    override func save() -> String {
        print("Validating Email...")
        return super.save()
    }
}

class SqlIjectionDecorator: FieldDecorator {
    override func save() -> String {
        print("Preventing SQL injection...")
        return super.save()
    }
}

func main(){
	var email = EmailField()
    print(email.save())
    
    print("==== Whith Decorators ====")
    var validation = EmailValidatioDecorator(email)
    var sqlInjection = SqlIjectionDecorator(validation)
    
    print(sqlInjection.save())
}

main()
